package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/robdaemon/gokv/server"
)

func main() {
	port := flag.Int("port", 4321, "Port number to bind to")
	bind := flag.String("bind", "0.0.0.0", "address to bind on")
	flag.Parse()

	if port == nil {
		// rare condition, usually flag.Parse() will catch,
		// but since we're going to deref a pointer shortly...
		fmt.Println("port argument not valid")
		flag.Usage()
		os.Exit(1)
	}

	if bind == nil {
		// rare condition, usually flag.Parse() will catch,
		// but since we're going to deref a pointer shortly...
		fmt.Println("bind argument not valid")
		flag.Usage()
		os.Exit(1)
	}

	log.Print("Starting gokv server")

	server.Serve(*bind, *port)
}
