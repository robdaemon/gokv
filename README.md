# gokv

Basic, in-memory key-value store.

## Compiling

Install Go 1.14.2

```
go get github.com/robdaemon/gokv

cd $HOME/go/src/github.com/robdaemon/gokv
go build
```

The executable will be in the current directory.

## Running

First compile, see above.

```
./gokv [-bind <bind addr>] [-port <port>]
```

example:

```
./gokv -bind 127.0.0.1 -port 4545
```

Defaults are a bind address of `0.0.0.0` and a port of `4321`

## Running the tests

```
go test -v ./...
```

### Adding coverage data

```
go test -v -coverpkg=./... ./...
```

