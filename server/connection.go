package server

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/robdaemon/gokv/storage"
)

// Transaction describes the active transaction's changes
type Transaction struct {
	ParentTransaction *Transaction

	Changes map[string]string
	Deletes map[string]struct{}
}

// Connection is the active connection for a user
type Connection struct {
	ActiveTransaction *Transaction

	conn net.Conn
}

func (c *Connection) writeString(s string) error {
	_, err := c.conn.Write([]byte(s))
	return err
}

func (c *Connection) writeStringLn(s string) error {
	_, err := c.conn.Write([]byte(s + "\r\n"))
	return err
}

// GetCommand will retrieve a command from the connection
func (c *Connection) GetCommand() (string, error) {
	cmd := ""
	connbuf := bufio.NewReader(c.conn)
	for {
		b, err := connbuf.ReadByte()
		if err != nil {
			return "", err
		}

		switch b {
		case 8:
			// handle backspace
			if len(cmd) > 0 {
				cmd = cmd[:len(cmd)-1]
			}
			c.conn.Write([]byte{32, 8})
		case 9:
			// ignore a tab
		case 10:
			// LF, end of input
			return cmd, nil
		case 13:
			// CR, ignore
		default:
			// c.conn.Write([]byte{b})
			cmd += string(b)
		}
	}
}

// HandleCommand will dispatch the command to the appropriate handlers
func (c *Connection) HandleCommand(command string) error {
	cmd := strings.Fields(command)

	if len(cmd) == 0 {
		return nil
	}

	switch strings.ToUpper(cmd[0]) {
	case "SET":
		// set key value
		if len(cmd) < 3 {
			c.writeStringLn("Not enough parameters to SET command, expected two")
		} else if len(cmd) > 3 {
			c.writeStringLn("Too many parameters to SET command, expected two")
		} else {
			c.SetValue(cmd[1], cmd[2])
		}
	case "GET":
		// get
		if len(cmd) < 2 {
			c.writeStringLn("Not enough parameters to GET command, expected one")
		} else if len(cmd) > 2 {
			c.writeStringLn("Too many parameters to GET command, expected one")
		} else {
			v, err := c.GetValue(cmd[1])
			if err != nil {
				c.writeStringLn(err.Error())
			} else {
				c.writeStringLn(v)
			}
		}
	case "DELETE":
		// delete a key
		if len(cmd) < 2 {
			c.writeStringLn("Not enough parameters to DELETE command, expected one")
		} else if len(cmd) > 2 {
			c.writeStringLn("Too many parameters to DELETE command, expected one")
		} else {
			if err := c.Delete(cmd[1]); err != nil {
				c.writeStringLn(err.Error())
			}
		}
	case "BEGIN":
		// start a transaction
		if len(cmd) > 1 {
			c.writeStringLn("Too many parameters to BEGIN command, expected zero")
		} else {
			c.BeginTransaction()
		}
	case "COMMIT":
		// commit transaction
		if len(cmd) > 1 {
			c.writeStringLn("Too many parameters to COMMIT command, expected zero")
		} else {
			c.CommitTransaction()
		}
	case "ROLLBACK":
		// rollback transaction
		if len(cmd) > 1 {
			c.writeStringLn("Too many parameters to ROLLBACK command, expected zero")
		} else {
			c.RollbackTransaction()
		}
	case "BYE":
		// end the connection
		return fmt.Errorf("connection ending")
	default:
		c.writeStringLn("Unknown command")
	}
	return nil
}

// SetValue will set a value in either the active transaction or the underlying store
func (c *Connection) SetValue(key, value string) error {
	var err error
	if c.ActiveTransaction != nil {
		c.ActiveTransaction.Changes[key] = value
		delete(c.ActiveTransaction.Deletes, key)
	} else {
		err = storage.GetInstance().Put(key, value)
	}

	return err
}

// GetValue will get a value in either the active transaction or the underlying store
func (c *Connection) GetValue(key string) (string, error) {
	t := c.ActiveTransaction
	for {
		if t == nil {
			break
		}

		if _, exists := t.Deletes[key]; exists {
			// being deleted within a transaction is a short circuit
			return "", fmt.Errorf("key does not exist")
		}

		if v, exists := t.Changes[key]; exists {
			return v, nil
		}

		t = t.ParentTransaction
	}

	// if we're here, we've exhausted the transactions and now
	// go to the underlying store
	return storage.GetInstance().Get(key)
}

// Delete will delete a key from the store
func (c *Connection) Delete(key string) error {
	var err error
	if c.ActiveTransaction != nil {
		c.ActiveTransaction.Deletes[key] = struct{}{}

		delete(c.ActiveTransaction.Changes, key)
	} else {
		err = storage.GetInstance().Delete(key)
	}

	return err
}

// BeginTransaction will start a new transaction on the current connection
func (c *Connection) BeginTransaction() {
	t := &Transaction{
		ParentTransaction: c.ActiveTransaction,
		Changes:           make(map[string]string),
		Deletes:           make(map[string]struct{}),
	}

	c.ActiveTransaction = t
}

// CommitTransaction will commit the active transaction, no op if nil
func (c *Connection) CommitTransaction() {
	active := c.ActiveTransaction
	if active == nil {
		return
	}

	parent := active.ParentTransaction
	if parent == nil {
		storage.GetInstance().Merge(active.Changes, active.Deletes)
	} else {
		for k := range active.Deletes {
			parent.Deletes[k] = struct{}{}
		}

		for k, v := range active.Changes {
			parent.Changes[k] = v
		}
	}

	c.ActiveTransaction = parent
}

// RollbackTransaction will rollback the active transaction, no op if nil
func (c *Connection) RollbackTransaction() {
	active := c.ActiveTransaction
	if active == nil {
		return
	}

	c.ActiveTransaction = active.ParentTransaction
}

// Handles incoming requests.
func handleRequest(conn net.Conn) {
	defer conn.Close()

	c := Connection{conn: conn}
	for {
		conn.Write([]byte("> "))

		cmd, err := c.GetCommand()
		if err != nil {
			log.Printf("Unable to read from socket: %#v", err)
			return
		}

		err = c.HandleCommand(cmd)
		if err != nil {
			log.Printf("Unable to dispatch command: %#v", err)
			return
		}
	}
}
