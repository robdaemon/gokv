package server

import (
	"fmt"
	"log"
	"net"
)

// Serve will start the TCP REPL-like server
func Serve(bindAddr string, port int) {
	addr := fmt.Sprintf("%s:%d", bindAddr, port)
	l, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatalf("Error listening: %#v", err)
	}
	defer l.Close()

	log.Printf("Listening on %s", addr)
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatalf("Unable to accept connection: %#v", err)
		}
		go handleRequest(conn)
	}
}
