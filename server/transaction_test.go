package server

import (
	"testing"

	"github.com/robdaemon/gokv/storage"
)

func cleaner() {
	storage.GetInstance().Clear()
}

func TestBeginCreatesTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("BEGIN")
	if err != nil {
		t.Error(err)
	}

	if conn.ActiveTransaction == nil {
		t.Errorf("ActiveTransaction should not be nil")
	}
}

func TestCommitEmptyTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("BEGIN")
	if err != nil {
		t.Error(err)
	}

	if conn.ActiveTransaction == nil {
		t.Errorf("ActiveTransaction should not be nil")
	}

	err = conn.HandleCommand("COMMIT")
	if err != nil {
		t.Error(err)
	}

	if conn.ActiveTransaction != nil {
		t.Errorf("ActiveTransaction should not be nil")
	}

	c, err := storage.GetInstance().KeyCount()
	if err != nil {
		t.Error(err)
	}

	if c != 0 {
		t.Errorf("Underlying store should be empty")
	}
}

func TestSetValueNoTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("SET a foo")
	if err != nil {
		t.Error(err)
	}

	value, err := conn.GetValue("a")
	if err != nil {
		t.Error(err)
	}

	if value != "foo" {
		t.Errorf("want foo, found %s", value)
	}

	// set value should have written directly to the underlying store
	value, err = storage.GetInstance().Get("a")
	if err != nil {
		t.Error(err)
	}

	if value != "foo" {
		t.Errorf("want foo, found %s", value)
	}
}

func TestSetValueSingleTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("BEGIN")
	if err != nil {
		t.Error(err)
	}

	err = conn.HandleCommand("SET a foo")
	if err != nil {
		t.Error(err)
	}

	value, err := conn.GetValue("a")
	if err != nil {
		t.Error(err)
	}

	if value != "foo" {
		t.Errorf("want foo, found %s", value)
	}

	// set value should NOT have written directly to the underlying store
	value, err = storage.GetInstance().Get("a")
	if err == nil {
		t.Error(err)
	}

	conn.CommitTransaction()
	// set value should have written directly to the underlying store
	value, err = storage.GetInstance().Get("a")
	if err != nil {
		t.Error(err)
	}

	if value != "foo" {
		t.Errorf("want foo, found %s", value)
	}

	if conn.ActiveTransaction != nil {
		t.Error("ActiveTransaction should be nil now")
	}
}

func TestRollbackSingleTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("BEGIN")
	if err != nil {
		t.Error(err)
	}

	err = conn.HandleCommand("SET a foo")
	if err != nil {
		t.Error(err)
	}

	value, err := conn.GetValue("a")
	if err != nil {
		t.Error(err)
	}

	if value != "foo" {
		t.Errorf("want foo, found %s", value)
	}

	// set value should NOT have written directly to the underlying store
	value, err = storage.GetInstance().Get("a")
	if err == nil {
		t.Error(err)
	}

	conn.RollbackTransaction()
	if conn.ActiveTransaction != nil {
		t.Error("ActiveTransaction should be nil")
	}

	value, err = conn.GetValue("a")
	if err == nil {
		t.Error("Should have gotten an error for a missing key")
	}

	// set value should NOT have written directly to the underlying store
	value, err = storage.GetInstance().Get("a")
	if err == nil {
		t.Error("should have gotten an error for missing key from main store")
	}
}

func TestRollbackNoTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("ROLLBACK")
	if err != nil {
		t.Error(err)
	}

	c, err := storage.GetInstance().KeyCount()
	if err != nil {
		t.Error(err)
	}

	if c != 0 {
		t.Errorf("Underlying store should be empty")
	}
}

func TestCommitNoTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.HandleCommand("COMMIT")
	if err != nil {
		t.Error(err)
	}

	c, err := storage.GetInstance().KeyCount()
	if err != nil {
		t.Error(err)
	}

	if c != 0 {
		t.Errorf("Underlying store should be empty")
	}
}

func TestGetDeletedWithinTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.SetValue("a", "foo")
	if err != nil {
		t.Error(err)
	}

	conn.BeginTransaction()

	err = conn.Delete("a")
	if err != nil {
		t.Error(err)
	}

	_, err = conn.GetValue("a")
	if err == nil {
		t.Error("getting a deleted key in a transaction should fail")
	}
}

func TestDeleteWithoutTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	err := conn.SetValue("a", "foo")
	if err != nil {
		t.Error(err)
	}

	err = conn.Delete("a")
	if err != nil {
		t.Error(err)
	}

	_, err = conn.GetValue("a")
	if err == nil {
		t.Error("getting a deleted key in a transaction should fail")
	}
}

func TestNestedTransaction(t *testing.T) {
	t.Cleanup(cleaner)

	conn := Connection{}

	conn.BeginTransaction()

	if err := conn.SetValue("a", "foo"); err != nil {
		t.Error(err)
	}

	if err := conn.SetValue("1234", "fooobar"); err != nil {
		t.Error(err)
	}

	conn.BeginTransaction()

	if err := conn.SetValue("a", "bar"); err != nil {
		t.Error(err)
	}

	value, err := conn.GetValue("a")
	if err != nil {
		t.Error(err)
	}
	if value != "bar" {
		t.Errorf("expected value bar, found %s", value)
	}

	value, err = conn.GetValue("1234")
	if err != nil {
		t.Error(err)
	}
	if value != "fooobar" {
		t.Errorf("expected value bar, found %s", value)
	}

	if err := conn.Delete("1234"); err != nil {
		t.Error(err)
	}

	value, err = conn.GetValue("1234")
	if err == nil {
		t.Errorf("expected the key 1234 to be deleted")
	}

	conn.CommitTransaction()

	value, err = conn.GetValue("a")
	if err != nil {
		t.Error(err)
	}
	if value != "bar" {
		t.Errorf("expected value bar, found %s", value)
	}

	value, err = conn.GetValue("1234")
	if err == nil {
		t.Errorf("expected the key 1234 to be deleted")
	}
}
