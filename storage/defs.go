package storage

// Engine is a generic interface for describing operations against a store
type Engine interface {
	Get(key string) (string, error)
	Put(key, value string) error
	Delete(key string) error
	Clear() error
	Merge(changes map[string]string, deletes map[string]struct{}) error
	KeyCount() (int, error)
}
