package storage

var data *InMemory

// GetInstance will return the Engine singleton
func GetInstance() Engine {
	return data
}

func init() {
	data = NewInMemory()
}
