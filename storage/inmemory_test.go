package storage

import "testing"

func TestGetMissingKey(t *testing.T) {
	instance := NewInMemory()

	v, err := instance.Get("foo")
	if err == nil {
		t.Errorf("expected an error for a missing key")
	}

	if v != "" {
		t.Errorf("expected empty string for missing key")
	}
}

func TestGetExistingKey(t *testing.T) {
	instance := NewInMemory()

	err := instance.Put("foo", "bar")
	if err != nil {
		t.Error(err)
	}

	v, err := instance.Get("foo")
	if err != nil {
		t.Error(err)
	}

	if v != "bar" {
		t.Errorf("expected bar found %s", v)
	}
}

func TestDeleteMissingKey(t *testing.T) {
	instance := NewInMemory()

	err := instance.Delete("foo")
	if err == nil {
		t.Errorf("expected an error for a missing key")
	}
}

func TestDeleteExistingKey(t *testing.T) {
	instance := NewInMemory()

	err := instance.Put("foo", "bar")
	if err != nil {
		t.Error(err)
	}

	err = instance.Delete("foo")
	if err != nil {
		t.Error(err)
	}

	v, err := instance.Get("foo")
	if err == nil {
		t.Errorf("expected an error for a missing key")
	}

	if v != "" {
		t.Errorf("expected empty string for missing key")
	}
}

func TestClear(t *testing.T) {
	instance := NewInMemory()

	err := instance.Put("foo", "bar")
	if err != nil {
		t.Error(err)
	}

	count, err := instance.KeyCount()
	if err != nil {
		t.Error(err)
	}

	if count != 1 {
		t.Errorf("expected 1 entry in the store, found %d", count)
	}

	err = instance.Clear()
	if err != nil {
		t.Error(err)
	}

	count, err = instance.KeyCount()
	if err != nil {
		t.Error(err)
	}

	if count != 0 {
		t.Errorf("expected 0 entries in the store, found %d", count)
	}
}

func TestMerge(t *testing.T) {
	instance := NewInMemory()

	err := instance.Put("foo", "bar")
	if err != nil {
		t.Error(err)
	}

	err = instance.Put("bar", "foo")
	if err != nil {
		t.Error(err)
	}

	count, err := instance.KeyCount()
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Errorf("expected 2 entries in the store, found %d", count)
	}

	err = instance.Merge(map[string]string{
		"foo": "quux",
		"new": "good",
	}, map[string]struct{}{
		"bar": struct{}{},
	})
	if err != nil {
		t.Error(err)
	}

	count, err = instance.KeyCount()
	if err != nil {
		t.Error(err)
	}

	if count != 2 {
		t.Errorf("expected 2 entries in the store, found %d", count)
	}

	v, err := instance.Get("foo")
	if err != nil {
		t.Error(err)
	}

	if v != "quux" {
		t.Errorf("expected quux found %s", v)
	}

	v, err = instance.Get("new")
	if err != nil {
		t.Error(err)
	}

	if v != "good" {
		t.Errorf("expected good found %s", v)
	}

	v, err = instance.Get("bar")
	if err == nil {
		t.Errorf("expected an error for a missing key")
	}

	if v != "" {
		t.Errorf("expected empty string for missing key")
	}
}
