package storage

import (
	"fmt"
	"sync"
)

// InMemory is a purely in-memory instance of the Engine interface
type InMemory struct {
	Engine

	data map[string]string
	lock sync.Mutex
}

// NewInMemory constructs a InMemory instance
func NewInMemory() *InMemory {
	return &InMemory{
		data: make(map[string]string),
	}
}

// Get will retrieve a key from the underlying datastore, returning an error if it doesn't exist
func (s *InMemory) Get(key string) (string, error) {
	s.lock.Lock()
	defer s.lock.Unlock()

	v, found := s.data[key]
	if !found {
		return "", fmt.Errorf("no such key %s", key)
	}
	return v, nil
}

// Put will add / update a key in the underlying datastore, replacing the value if it already exists
func (s *InMemory) Put(key, value string) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	s.data[key] = value

	return nil
}

// Delete will remove an individual key from the underlying store, returning an error if it doesn't exist
func (s *InMemory) Delete(key string) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	_, found := s.data[key]
	if !found {
		return fmt.Errorf("no such key %s", key)
	}

	delete(s.data, key)
	return nil
}

// Clear will drop the existing store and create a new, empty one
func (s *InMemory) Clear() error {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.data = make(map[string]string)

	return nil
}

// Merge will merge a set of changes into the store in a single operation
func (s *InMemory) Merge(changes map[string]string, deletes map[string]struct{}) error {
	s.lock.Lock()
	defer s.lock.Unlock()

	for k := range deletes {
		delete(s.data, k)
	}

	for k, v := range changes {
		s.data[k] = v
	}

	return nil
}

// KeyCount will return the number of keys in the store
func (s *InMemory) KeyCount() (int, error) {
	return len(s.data), nil
}
